#!/bin/bash

crontab -l > crons
cat crons | grep "monitoring" > /dev/null
if [ $? == 1 ]; then
	echo "[Warning] Monitoring is not running."
else
	sed -i '/monitoring/d' crons
	crontab crons
	mysql_config_editor remove --login-path=monitorlogin
	echo "[Success] Monitoring stopped."
fi

rm crons
