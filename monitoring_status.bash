#!/bin/bash

crontab -l > crons
cat crons | grep "monitoring" > /dev/null
if [ $? == 1 ]; then
	echo "Monitoring is not running."
else
	echo "Monitoring is running."
fi
rm crons
