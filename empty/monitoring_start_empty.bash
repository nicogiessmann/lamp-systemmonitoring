#!/bin/bash
echo "Connect to db syslog as user@localhost..."
mysql_config_editor set --login-path=monitorlogin --host=localhost --user=user --password

echo "quit" | mysql --login-path=monitorlogin > error 2>&1
if [ -s error ]; then
	echo "[Warning] Password not correct. Monitoring is not running."
	mysql_config_editor remove --login-path=monitorlogin
else
	crontab -l > crons
	cat crons | grep 'monitoring' > /dev/null
	if [ $? == 1 ]; then
		echo "*/5 * * * * $dir" >> crons
		crontab crons
		echo "[Success] Monitoring is running."
	else
		echo "[Warning] Monitoring is running. Stop monitoring before restart."
	fi

	rm crons
fi

rm error
