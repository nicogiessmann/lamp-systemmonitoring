#!/bin/bash
function getCpu() {
	idle_dec=$(iostat | sed -n '4p' | tr -s ' ' | cut -d ' ' -f7 | tr ',' '.')
	cpu=$(echo '100 - '$idle_dec | bc)
	echo $cpu
}
function getRam() {
	ram_alldata=$(free | sed -n '2p')
	ram_total=$(echo $ram_alldata | cut -d ' ' -f2)
	ram_used=$(echo $ram_alldata | cut -d ' ' -f3)
	ram=$(echo 'scale=2;' $(($ram_used*100)) '/' $ram_total | bc)
	echo $ram
}
function getSwap() {
	swap_alldata=$(free | sed -n '3p')
	swap_total=$(echo $swap_alldata | cut -d ' ' -f2)
	swap_used=$(echo $swap_alldata | cut -d ' ' -f3)
	swap=$(echo 'scale=2;' $(($swap_used*100)) '/' $swap_total | bc)
	echo $swap
}
function getDisk() {
	disk=$(df -hT / | sed -n '2p' | tr -s ' ' | cut -d ' ' -f6 | tr -d '%')
	echo $disk
}

echo $(whoami) $(date) 'Cpu:' $(getCpu)'% Ram:' $(getRam)'% Swap:' $(getSwap)'% Disk:' $(getDisk)'%' >> $dir/sys.log
echo "insert into syslog (host, cpu, ram, swap, disk) values ('$(whoami)','$(getCpu)','$(getRam)','$(getSwap)','$(getDisk)');" | mysql --login-path=monitorlogin monitoring >> $dir/error.log 2>&1
