#!/bin/bash
# Manuelles Installationsskript LAMP Systemmonitoring
# Autor: Nico Gießmann (c) 2019
# Verfügbar unter git-Repository: https://gitlab.com/nicogiessmann/lamp-systemmonitoring 
# Starte Skript installation.bash in Verzeichnis lamp-systemmonitoring/

# Generiere Skripte
#current_directory=$(pwd)
#current_directory=$(echo $current_directory | sed 's/\//\\\//g')
#sed 's/$dir/'$current_directory'/g' empty/monitoring_empty.bash > monitoring.bash
#sed 's/$dir/'$current_directory'/g' empty/monitoring_start_empty.bash > monitoring_start.bash

# Installiere LAMP-Pakete
#sudo apt-get update
#sudo apt-get install apache2 libapache2-mod-php7.2 php7.2-mysql mysql-server

# Starte MySQL
#sudo service mysql start

# Anlegen von Benutzer user
read -p "Passwort für Nutzer user@localhost festlegen:" -s pass;
echo
echo "create user 'user'@'localhost' identified by '$pass'; quit;" | sudo mysql

# Tabelle syslog erstellen
#sudo echo "create database monitoring; use monitoring; create table syslog (host char(255) not null, time timestamp not null default current_timestamp, cpu decimal(5,2) not null, ram decimal(5,2) not null, swap decimal(5,2) not null, disk tinyint not null); grant insert, select on monitoring.syslog to 'user'@'localhost'; flush privileges; quit;" | mysql

#chmod u+x monitoring_start.bash, monitoring.bash, monitoring_status.bash, monitoring_stop.bash

#sudo cp syslog.php /var/www/html/

#sudo service apache2 start

