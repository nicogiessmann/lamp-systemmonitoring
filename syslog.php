<!DOCTYPE html>
<html>
<head>
<title>Monitoring</title>
</head>
<body>
<h1>Welcome to the monitoring application</h1>
<p>Connect to db syslog as user@localhost</p>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
Password: <input type="password" name="password">
<br>
<input type="submit">
</form>
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$servername = "localhost";
	$username = "user";
	$password = $_POST["password"];
	$dbname = "monitoring";
	$conn = new mysqli($servername, $username, $password, $dbname);
	echo "<br>";
	if ($conn->connect_error) {
        	die("Connection failed: " . $conn->connect_error);
	}
	echo "[Success] Connection and login.<br>";
	$sql = "select * from syslog order by time desc limit 5";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
        	while($row = $result->fetch_assoc()) {
                	echo $row["host"] . " " . $row["time"] . " Cpu: " . $row["cpu"] . "% Ram:  " . $row["ram"] . "% Swap: " . $row["swap"] . "% Disk: " . $row["disk"] . "%<br>";
        	}
	} else {
        	echo "0 results";
	}
}
?>
</body>
</html>
